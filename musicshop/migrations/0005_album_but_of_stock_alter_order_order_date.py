# Generated by Django 4.0.4 on 2022-04-29 13:00

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('musicshop', '0004_alter_cart_final_price_alter_order_order_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='but_of_stock',
            field=models.IntegerField(default=False, verbose_name='Нет в наличии'),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_date',
            field=models.DateField(default=datetime.datetime(2022, 4, 29, 13, 0, 25, 109373, tzinfo=utc), verbose_name='Дата получения заказа'),
        ),
    ]
